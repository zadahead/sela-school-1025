# UIkit - Elements

_Elements are all that user interacts with. and sees. if its a button, an icon or a text. all can be concider an element. and it is best to make it as reusable as possible and have one code base to each element_



## Our Mission


1. Create a `<Icon />` element.
1. it will show and icon baed on the `icon` prop been passed. 


## Now You Try

1. update the `<Btn />` tag.
1. make it so, it will Include an icon if an `icon` prop has passed. 


## Now You Are an Expert

1. create a `<Checbox />` tag.
1. it will work as a checkbox, to mark selected item as `true` or `false`. 

