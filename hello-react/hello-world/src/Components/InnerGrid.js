import Grid from './UIKit/Layouts/Grid/Grid';
import Line, { Between, Rows } from './UIKit/Layouts/Line/Line';

const InnerGrid = () => {
    return (
        <Grid className="innerGrid">
            <div>
                <Between>
                    <div>Top Referrals</div>
                    <div>...</div>
                </Between>
            </div>
            <Rows>
                <Between>
                    <Line>
                        <div>icon</div>
                        <div>Github</div>
                    </Line>
                    <div>53,000</div>
                </Between>
                <Between>
                    <Line>
                        <div>icon</div>
                        <div>Github</div>
                    </Line>
                    <div>53,000</div>
                </Between>
                <Between>
                    <Line>
                        <div>icon</div>
                        <div>Github</div>
                    </Line>
                    <div>53,000</div>
                </Between>
                <Between>
                    <Line>
                        <div>icon</div>
                        <div>Github</div>
                    </Line>
                    <div>53,000</div>
                </Between>
            </Rows>
        </Grid>

    )
}

export default InnerGrid;