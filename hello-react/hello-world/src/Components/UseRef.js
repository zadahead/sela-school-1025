import { useRef, useState } from "react";
import { useEffect } from "react/cjs/react.development";

const UseRef = () => {
    //POS: 1
    const [count, setCount] = useState(0);

    const wrapperRef = useRef();
    const countRef = useRef(0);

    console.log('countRef-onload', countRef.current);

    useEffect(() => {
        console.log('wrapperRef', wrapperRef.current.offsetWidth);
        console.log('countRef', countRef.current);
    }, [])

    const handleAddCountRef = () => {
        //POS: 3
        countRef.current++;
        console.log(countRef.current);
    }

    //POS: 2
    return (
        <div ref={wrapperRef} className="wrapper">
            <h1>Hello Ref, Count Ref: {countRef.current}</h1>
            <button onClick={handleAddCountRef}>Add Count Ref</button>
            <button onClick={() => setCount(count + 1)}>Add Count</button>
        </div>
    )
}

export const UseRefInput = () => {
    //Uncontrolled!!!!!!

    const inputRef = useRef();

    const handleShowValue = () => {
        console.log(inputRef.current.value);
    }

    const handleUpdate = () => {
        inputRef.current.value = "Helllo";
    }

    return (
        <div>
            <input ref={inputRef} />
            <button onClick={handleShowValue}>Show Value</button>
            <button onClick={handleUpdate}>Update</button>
        </div>
    )
}

export default UseRef;