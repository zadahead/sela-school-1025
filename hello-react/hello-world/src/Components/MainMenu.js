
import { useState } from 'react';

const MainMenu = () => {
    const [selected, setSelected] = useState(null);
    const colors = ['yellow', 'green', 'blue'];


    const handleSelected = (index) => {
        setSelected(index);
    }

    const renderPages = () => {
        switch (selected) {
            case 0: return <PageA color={colors[selected]} />
            case 1: return <PageB color={colors[selected]} />
            case 2: return <PageC color={colors[selected]} />
            default: return null;
        }
    }

    const renderButtons = () => {
        const pages = ['PageA', 'PageB', 'PageC'];

        return pages.map((page, index) => {
            return <button key={page} style={renderButtonStyle(index)} onClick={() => handleSelected(index)}>{page}</button>
        })
    }

    const renderButtonStyle = (index) => {
        if (index === selected) {
            return {
                backgroundColor: colors[index],
                color: 'red'
            }
        }
        return {
            backgroundColor: 'gray',
            color: 'black'
        }
    }

    return (
        <div>
            <h1>MainMenu</h1>
            <div>
                {renderButtons()}
            </div>
            <div>
                {renderPages()}
            </div>
        </div>
    )
}


const Page = (props) => {
    return (
        <div style={{ backgroundColor: props.color }}>
            {props.children}
        </div>
    )
}

const PageA = (props) => {
    return (
        <Page {...props}>
            <h2>PageA</h2>
        </Page>
    )
}

const PageB = (props) => {
    return (
        <Page {...props}>
            <h2>PageB</h2>
        </Page>
    )
}

const PageC = (props) => {
    return (
        <Page {...props}>
            <h2>PageC</h2>
        </Page>
    )
}

export default MainMenu;