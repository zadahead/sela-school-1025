const PrettyThing = (props) => {
    console.log(props);

    const styleCss = {
        color: 'red',
        padding: '10px'
    }

    return (
        <div style={styleCss}>
            <div>*********************</div>
            <div>****** {props.name} *********</div>
            <div>****** {props.children} *********</div>
            <div>*********************</div>
        </div>
    )
}

/*
    <Article title="my title" subject="this is info">
        <div>......</div>
    </Article>
*/

export default PrettyThing;