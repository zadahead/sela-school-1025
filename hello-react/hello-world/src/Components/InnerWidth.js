import { useInnerWidth } from "Hooks/useInnerWidth";

export const InnerWidth = () => {
    //logic
    const width = useInnerWidth();

    //render / view
    return (
        <div>
            <h1>{width}</h1>
        </div>
    )
}

