import { useState } from 'react';

/*
    <Toggler />
        <button>Toggle</button> -> show / hide child component
        <UserDisplay />
*/

const Toggler = (props) => {
    const [isDisplay, setIsDisplay] = useState(false);

    const handleToggle = () => {
        setIsDisplay(!isDisplay);
    }

    return (
        <div>
            <h1>Toggler</h1>
            <button onClick={handleToggle}>Toggle</button>
            {isDisplay && props.children}
        </div>
    )
}

export default Toggler;