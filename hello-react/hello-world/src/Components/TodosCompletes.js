import { useSelector } from "react-redux"

export const TodosCompleted = () => {
    const todos = useSelector(state => state.todos);

    const completed = todos.filter(i => i.completed).length;

    return (
        <div>
            <h5>Completed, {completed}</h5>
        </div>
    )
}