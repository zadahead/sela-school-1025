import React from 'react';

class LifeCycleClass extends React.Component {
    state = {
        count: 0
    }

    handleAdd = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    componentDidMount = () => {
        //loading of async data
        //set initial state
        //bind events
        console.log('componentDidMount');
    }

    componentDidUpdate = () => {
        //post/patch async data
        //upate other states
        console.log('componentDidUpdate');
    }

    componentWillUnmount = () => {
        //notify before close, unsaved
        //clean up listener events
        //unbind events
        console.log('componentWillUnmount');
    }

    render = () => {
        console.log('Render');

        return (
            <div>
                <h1>LifeCycleClass, {this.state.count}</h1>
                <button onClick={this.handleAdd}>Add</button>
            </div>
        )
    }
}

export default LifeCycleClass;