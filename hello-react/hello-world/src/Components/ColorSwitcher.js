import { useState } from "react";

const ColorSwitcher = () => {
    console.log('Start');
    const [isRed, setIsRed] = useState(false);

    console.log('Current isRed:', isRed);

    const handleSwitchColor = () => {
        console.log('HandleSwitchColor');
        setIsRed(!isRed);
    }

    const styleCSS = {
        color: isRed ? 'red' : 'blue'
    }

    console.log('Render', styleCSS.color);
    return (
        <div>
            <h1 style={styleCSS}>COLOR!</h1>
            <button onClick={handleSwitchColor}>Switch Color</button>
        </div>
    )
}

export default ColorSwitcher;