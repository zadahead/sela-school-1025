
import { useEffect, useState } from "react";

/*
    useState
    useEffect 


    didMount
    didUpdate
    willUnmount

    willUpdate
    didStateUpdate
    willStateUpdate
*/

const LifeCycleFunc = () => {
    const [count, setCount] = useState(0);
    const [isRed, setIsRed] = useState(false);

    useEffect(() => {
        console.log('didMount');

        return () => {
            console.log('willUnmount');
        }
    }, []) // didMount ===> [], return () => willUnmount

    useEffect(() => {
        console.log('didUpdate -> all');

        return () => {
            console.log('state willUpdate -> all');
        }
    }) // didUpdate ===> no arguments, return () => willUpdate

    useEffect(() => {
        console.log('count didStateUpdate -> count: ', count);
        return () => {
            console.log('count willStateUpdate -> count', count);
        }
    }, [count]) // didStateUpdate ===> [count], return () => willStateUpdate

    const handleAdd = () => {
        setCount(count + 1);
    }

    const styleCss = {
        color: isRed ? 'red' : 'blue'
    }

    // console.log('render');
    return (
        <div>
            <h1 style={styleCss}>LifeCycleFunc, {count}</h1>
            <button onClick={handleAdd}>Add</button>
            <button onClick={() => setIsRed(!isRed)}>Change Color</button>
        </div>
    )
}

export default LifeCycleFunc;