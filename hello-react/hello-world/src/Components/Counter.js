import { useCounter } from "Hooks/useCounter";
import { useState } from "react";

const CounterWrap = () => {
    const [count, setCount] = useState(10); //0

    console.log('RENDER');

    const handleAdd = () => {
        setCount(count + 1);
    }

    return (
        <div>
            <CounterFunc count={count} setCount={handleAdd} />
            {/* <CounterFunc isSwitcher={true} count={count} setCount={handleAdd} /> */}
        </div>
    )
}

const CounterFunc = (props) => {
    const [isRed, setIsRed] = useState(true);

    const handleSwitch = () => {
        setIsRed(!isRed);
    }

    const styleCss = {
        color: isRed ? 'red' : 'blue'
    }


    return (
        <div>
            <h1 style={styleCss}>Count: {props.count}</h1>
            <button onClick={props.setCount}>Add</button>
            {props.isSwitcher && <button onClick={handleSwitch}>Switch</button>}
        </div>
    )
}


export const Counter = () => {
    //logic
    const [counter, onClick] = useCounter();

    //view - render
    return (
        <div>
            <h1>Count, {counter}</h1>
            <button onClick={onClick}>Add</button>
        </div>
    )
}

export const MultipleCounters = () => {
    return (
        <div>
            <Counter />
            <Counter />
            <Counter />
        </div>
    )
}

export default CounterWrap;