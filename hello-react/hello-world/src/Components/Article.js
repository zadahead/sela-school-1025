const style = {
    wrap: {
        padding: '10px',
        border: '1px solid #333'
    },
    h1: {
        margin: '0'
    },
    content: {
        backgroundColor: '#e1e1e1',
        padding: '10px',
        border: '1px solid #d1d1d1'
    }
}

const Article = (props) => {
    return (
        <div style={style.wrap}>
            {props.title && <h1 style={style.h1}>{props.title}</h1>}
            {props.subject && <h2>{props.subject}</h2>}
            <div style={style.content}>
                {props.children}
            </div>
        </div>
    )
}

export default Article;