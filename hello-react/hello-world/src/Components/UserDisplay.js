/*

    user logged in ? 

        <h1>Hello User! #NAME$</h1>

    user not logged in

        <h1>You need to log in</h1>

*/


const UserDisplay = (props) => {
    if (props.isLogin) {
        return <h1>Hello User! {props.name}</h1>
    }
    return <h1>You need to log in</h1>;
}

export default UserDisplay;