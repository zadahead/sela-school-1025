import { useState } from 'react';

const User = (props) => {
    const [isShow, setIsShow] = useState(true);

    const handleToggleShow = () => {
        setIsShow(!isShow);
    }

    const renderActionTitle = () => {
        return isShow ? 'Hide' : 'Show';
    }

    return (
        <div>
            <h2>
                <span>{props.name}&nbsp;</span>
                {isShow && <span>{props.age}&nbsp;</span>}
                <button onClick={handleToggleShow}>
                    {renderActionTitle()} Age
                </button>
            </h2>
        </div>
    )
}

export default User;