
import User from './User';

const list = [
    { id: 1, name: 'mosh', age: 25 },
    { id: 2, name: 'david', age: 35 },
    { id: 3, name: 'ron', age: 25 },
    { id: 4, name: 'shraga', age: 25 }
]

/*
    basic
    <UsersList />
        <User name age /> ------ '#Name# #Age#'
        <User />
        <User />
        <User />
        <User />

    expert
    <UsersList />
        <User name age /> -----  '#Name# (show age)' ==> '#Name# #Age# (hide age)'
        <User />
        <User />
        <User />
        <User />
*/

const UsersList = () => {

    const renderList = () => {
        return list.map((item) => {
            return (
                <User key={item.id} name={item.name} age={item.age} />
            );
        })
    }
    return (
        <div>
            <h1>Users List:</h1>
            <div>
                {renderList()}
            </div>
        </div>
    )
}

export default UsersList;