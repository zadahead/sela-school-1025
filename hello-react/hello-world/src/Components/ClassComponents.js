
import React from 'react';

export class Counter extends React.Component {
    state = {
        count: 0,
    }

    addCount = () => {
        const newCount = this.state.count + 1;

        this.setState({
            count: newCount
        })
    }

    render = () => {
        return (
            <div>
                <h1>Count: {this.state.count}</h1>
                <button onClick={this.addCount}>Add</button>
            </div>
        )
    }
}


export class Toggler extends React.Component {
    state = {
        isRed: true
    }

    handleToggle = () => {
        this.setState({
            isRed: !this.state.isRed
        })
    }

    render = () => {
        const styleCss = { color: this.state.isRed ? 'red' : 'blue' }

        return (
            <div>
                <h1 style={styleCss}>Toggler</h1>
                <button onClick={this.handleToggle}>Toggle</button>
            </div>
        )
    }
}
