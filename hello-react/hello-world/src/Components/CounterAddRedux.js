import { Btn } from 'UIKit';

import { addCount } from 'State/counter';
import { useDispatch } from 'react-redux';


const CounterAddRedux = () => {
    const dispatch = useDispatch();

    const handleAdd = () => {
        dispatch(addCount())
    }

    return (
        <div>
            <Btn onClick={handleAdd}>Add</Btn>
        </div>
    )
}

export default CounterAddRedux;