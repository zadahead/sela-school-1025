
import axios from "axios";
import { useEffect, useState } from "react";


const useFetch = (url) => {
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState('');
    const [data, setData] = useState([]);

    useEffect(() => {
        const source = axios.CancelToken.source();

        axios
            .get(url, {
                cancelToken: source.token
            })
            .then(resp => {
                console.log(resp.data);
                setIsLoading(false);
                setData(resp.data);
            })
            .catch(err => {
                console.log(err);
                if (err.message !== 'axios') {
                    setIsLoading(false);
                    setError(err.message);
                }
            })

        return () => {
            //should cancel axios request here!
            source.cancel('axios');
        }
    }, [])

    const render = () => {
        if (isLoading) {
            return <h1>loading...</h1>
        }

        if (error) {
            return <h1>Error...</h1>
        }
        return null;
    }

    return [
        render(),
        data
    ]
}

const AxiosView = () => {
    //logic
    const [render, users] = useFetch('https://jsonplaceholder.typicode.com/users');

    //render view
    if (render) {
        return render;
    }

    return (
        <div>
            <h1>AxiosView</h1>
            <div>
                {users.map(u => (
                    <h3 key={u.id}>{u.name}</h3>
                ))}
            </div>
        </div>
    )
}

export default AxiosView;