import { useState } from "react";
import { Dropdown } from "UIKit";

const list = [
    { id: 1, value: 'mosh' },
    { id: 2, value: 'david1' },
    { id: 3, value: 'david2' },
    { id: 4, value: 'david3' },
    { id: 5, value: 'david4' },
    { id: 6, value: 'david5' },
    { id: 7, value: 'david6' }
]

const DropDownView = () => {
    const [selectedId, setSelectedId] = useState(6);

    return (
        <div>
            <h1>DropDown View</h1>
            <Dropdown list={list} selectedId={selectedId} onChange={setSelectedId} />
        </div>
    )
}

export default DropDownView;