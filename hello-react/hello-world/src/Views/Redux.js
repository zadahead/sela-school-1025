import CounterAddRedux from "Components/CounterAddRedux";
import { useSelector } from "react-redux";



const Redux = () => {
    const count = useSelector(state => state.count);
    console.log(count);

    return (
        <div>
            <h1>Redux, {count}</h1>
            <CounterAddRedux />
        </div>
    )
}

export default Redux;