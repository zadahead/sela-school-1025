import { useState } from 'react';
import { Checkbox, MultiSelect, RadioBtn } from 'UIKit';


//RadioBtn

const rawList = [
    { id: 1, value: 'mosh' },
    { id: 2, value: 'mosh' },
    { id: 3, value: 'mosh' },
    { id: 4, value: 'mosh' },
    { id: 5, value: 'mosh' }
]
const Home = () => {
    const [selectedId, setSelectedId] = useState(null);

    return (
        <div>
            <h1>Welcome Home</h1>
            <RadioBtn
                list={rawList}
                selectedId={selectedId}
                onChange={setSelectedId}
            />
        </div>
    )
}

//Multi Select

const rawListMulti = [
    { id: 1, value: 'mosh', selected: true },
    { id: 2, value: 'mosh', selected: false },
    { id: 3, value: 'mosh', selected: false },
    { id: 4, value: 'mosh', selected: true },
    { id: 5, value: 'mosh', selected: true }
]
export const HomeMultiSelect = () => {
    const [list, setList] = useState(rawListMulti);

    return (
        <div>
            <h1>Welcome Home</h1>
            <MultiSelect list={list} onChange={setList} />
        </div>
    )
}

//Checkbox

export const HomeCheckbox = () => {
    const [checked, setChecked] = useState(true);

    return (
        <div>
            <h1>Welcome Home</h1>
            <Checkbox checked={checked} onChange={setChecked}>Select this</Checkbox>
        </div>
    )
}


export default Home;