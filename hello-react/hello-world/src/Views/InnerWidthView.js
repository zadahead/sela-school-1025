import { Counter } from 'Components/Counter';
import { InnerWidth } from 'Components/InnerWidth';

const InnerWidthView = () => {
    return (
        <div>
            <h1>InnerWidthView</h1>
            <InnerWidth />
            <Counter />
        </div>
    )
}

export default InnerWidthView;