import { useInput } from 'Hooks/useInput';
import { useState } from 'react';
import { Rows, Btn, Input } from 'UIKit';

const Login = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const handleUsername = (e) => {
        setUsername(e.target.value);
    }

    const handlePassword = (e) => {
        setPassword(e.target.value);
    }

    const handleClick = () => {
        console.log('clicked -->', username, password);
    }

    return (
        <Rows>
            <Input value={username} onChange={handleUsername} i="user" placeholder="username" />
            <Input value={password} onChange={handlePassword} type="password" i="key" placeholder="password" />

            <Btn i="heart" onClick={handleClick}>Click Me</Btn>
        </Rows>
    )
}



export const LoginCustomHook = () => {
    //logic
    const username = useInput('starter', 'username...');
    const password = useInput('', 'password...');

    const handleLogin = () => {
        console.log(username.value, password.value);
    }

    //view
    return (
        <div style={{ marginTop: '100px' }}>
            <h1>{window.innerWidth}</h1>
            <Input {...username} />
            <Input {...password} />

            <Btn onClick={handleLogin}>Login</Btn>
        </div>
    )
}



/*

1) create innerWidth view, and route
2) create <InnerWidth /> component (it will display innerWidth, even on resize)
3) create useInnerWidth custom hook, the <InnerWidth /> will use. 

helpers: 

1) window.innerWidth ---- show inner width
2) window.addEventListener('resize', func) -- listener for resize

*/

export default Login;
