import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getTodos, patchTodos } from "State/todos";

const TodosView = () => {
    const todos = useSelector(state => state.todos);
    const dispatch = useDispatch();

    console.log(todos);

    useEffect(() => {
        dispatch(getTodos());
    }, [])

    const handlePatchTodos = (id) => {
        dispatch(patchTodos(id));
    }

    return (
        <div>
            <h1>TodosView</h1>
            <div>
                {todos.map(i => (
                    <h3 key={i.id} onClick={() => handlePatchTodos(i.id)} style={{
                        color: i.completed ? '#e1e1e1' : '#333'
                    }}>{i.title}</h3>
                ))}
            </div>
        </div>
    )
}

export default TodosView;