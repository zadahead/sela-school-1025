
import { GridRows } from './UIKit/Layouts/Grid/Grid';
import Line, { Between } from './UIKit/Layouts/Line/Line';

import './App.css';
import Icon from './UIKit/Elements/Icon/Icon';

import { Routes, Route, NavLink } from 'react-router-dom';

import Login, { LoginCustomHook } from './Views/Login';
import Home from './Views/Home';
import DropDownView from 'Views/DropDownView';
import InnerWidthView from 'Views/InnerWidthView';
import AxiosView from 'Views/AxiosView';
import Redux from 'Views/Redux';
import TodosView from 'Views/TodosView';
import { TodosCompleted } from 'Components/TodosCompletes';


const App = () => {

    return (
        <div className="App">
            <GridRows>
                <div>
                    <Between>
                        <Line>
                            <div>logo</div>
                            <Icon i="heart" />
                            <TodosCompleted />
                        </Line>
                        <Line>
                            <NavLink to="/todos">todos</NavLink>
                            <NavLink to="/redux">redux</NavLink>
                            <NavLink to="/api">api</NavLink>
                            <NavLink to="/innerWidth">innerWidth</NavLink>
                            <NavLink to="/hooks">hooks</NavLink>
                            <NavLink to="/dropdown">dropdown</NavLink>
                            <NavLink to="/login">Login</NavLink>
                        </Line>
                    </Between>
                </div>
                <div>
                    <Routes>
                        <Route path="/todos" element={<TodosView />} />
                        <Route path="/redux" element={<Redux />} />
                        <Route path="/api" element={<AxiosView />} />
                        <Route path="/innerWidth" element={<InnerWidthView />} />
                        <Route path="/hooks" element={<LoginCustomHook />} />
                        <Route path="/login" element={<Login />} />
                        <Route path="/home" element={<Home />} />
                        <Route path="/dropdown" element={<DropDownView />} />
                    </Routes>
                </div>
                <Line>
                    <div>Footer</div>
                </Line>
            </GridRows>
        </div>
    )
}

export default App;