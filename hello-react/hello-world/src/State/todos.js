import axios from "axios";

//actions 

export const getTodos = () => {
    return async (dispatch) => {
        axios
            .get('https://jsonplaceholder.typicode.com/todos')
            .then(resp => {
                dispatch({
                    type: 'GET_TODOS',
                    payload: resp.data
                })
            })
    }
}

export const patchTodos = (id) => {
    return {
        type: 'PATCH_TODOS',
        payload: id
    }
}


//reducers 

export const todosReducer = (state = [], action) => {
    switch (action.type) {
        case 'GET_TODOS': return action.payload.splice(0, 10)
        case 'PATCH_TODOS': {
            const index = state.findIndex(i => i.id === action.payload);
            const newState = [...state];
            newState[index].completed = !newState[index].completed;
            return newState;
        }

        default: return state;
    }
}