//actions
export const addCount = () => {
    return {
        type: 'ADD',
        payload: 1
    }
}


//reducers 
export const countReducer = (state = 0, action) => {
    //console.log('COUNT', action);

    switch (action.type) {
        case 'ADD': return state + action.payload;
        default: return state;
    }
}