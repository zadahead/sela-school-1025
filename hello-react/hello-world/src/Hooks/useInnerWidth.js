import { useEffect, useState } from "react";

export const useInnerWidth = () => {
    //logic
    const [innerWidth, setInnerWidth] = useState(0);

    useEffect(() => {
        handleSetWidth();
        window.addEventListener('resize', handleSetWidth);
        return () => {
            window.removeEventListener('resize', handleSetWidth);
        }
    }, [])

    const handleSetWidth = () => {
        setInnerWidth(window.innerWidth);
    }

    return innerWidth;
}